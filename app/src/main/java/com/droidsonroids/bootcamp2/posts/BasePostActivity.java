package com.droidsonroids.bootcamp2.posts;

import android.support.v7.app.AppCompatActivity;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

public abstract class BasePostActivity extends AppCompatActivity {

	protected static final String POSTS_URL = "https://api.parse.com/1/classes/posts";
	protected final OkHttpClient mHttpClient = new OkHttpClient();
	protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	protected Request.Builder getRequestBuilderWithHeaders() {
		return new Request.Builder()
				.addHeader("X-Parse-Application-Id", "UrL13QNO5BmD4jF0EcOXwowTMitpfwfL1REPfhEv")
				.addHeader("X-Parse-REST-API-Key", "6FLzVDCP6AGARSugzDzG8Ct50KQOVhoYUE7TpGpg");
	}
}
